﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
    public void Quit()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
    public void Retry()
    { 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
    }
    
}
