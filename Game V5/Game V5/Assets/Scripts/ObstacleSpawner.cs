﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject objectPrefab;
    public GameObject player;
    public float spawnDecreaseValue = 0.0002f;
    public float timeBetweenWaves = 1f;
    private float timeToSpawn = 0.1f;


    void FixedUpdate()
    {
        transform.position = new Vector3(0, 0, 100 + player.transform.position.z);
    }


    void Update()
    {
        if (Time.time >= timeToSpawn)
        {
            SpawnObjects();
            timeToSpawn = Time.time + timeBetweenWaves;
            timeBetweenWaves -= spawnDecreaseValue;
        }

    }

    void SpawnObjects()
    {
        int randomLineOrPrefab = Random.Range(1, 100);
        int randomIndex = Random.Range(0, spawnPoints.Length - 1);


        if (randomLineOrPrefab < 70)
        {

            for (int index = 0; index < spawnPoints.Length; index++)
            {
                if (randomIndex != index && randomIndex + 1 != index)
                {
                    Instantiate(objectPrefab, spawnPoints[index].position, Quaternion.identity);
                }
            }
        }
        else 
        {
            int randomPrefab = Random.Range(0, 5);
            PreDefinedObstacles(randomPrefab, randomIndex);
        }
    }

    void PreDefinedObstacles(int preset, int randomIndex)
    {
        switch (preset)
        {
            case 0:
                {
                    for (int index = 0; index < spawnPoints.Length; index++)
                        if (randomIndex != index && randomIndex + 1 != index)
                        {
                            Instantiate(objectPrefab,
                                spawnPoints[index].position + new Vector3(0, 0, index + 1),
                                Quaternion.identity);
                        }
                            


                }
                break;
            case 1:
                {
                    for (int index = 0; index < spawnPoints.Length; index++)
                        if (randomIndex != index && randomIndex + 1 != index)
                        {
                            Instantiate(objectPrefab,
                                spawnPoints[index].position + new Vector3(0, 0, index * - 1),
                                Quaternion.identity);
                        }

                }
                break;
            case 2:
                {
                    for (int index = 0; index < spawnPoints.Length; index++)
                        if (randomIndex != index && randomIndex + 1 != index)
                        {
                            Instantiate(objectPrefab,
                                spawnPoints[index].position + new Vector3(0, index, index * -2),
                                Quaternion.identity);
                        }

                }
                break;
            case 3:
                {
                    for (int index = 0; index < spawnPoints.Length; index++)
                        if (randomIndex != index && randomIndex + 1 != index)
                        {
                            Instantiate(objectPrefab,
                                spawnPoints[index].position + new Vector3(0, index, index * 2),
                                Quaternion.identity);
                        }

                }
                break;
            case 4:
                {
                    for (int index = 0; index < spawnPoints.Length; index++)
                        if (randomIndex != index && randomIndex + 1 != index)
                        {
                            Instantiate(objectPrefab,
                                spawnPoints[index].position + new Vector3(0, index + index*3, index + index%2),
                                Quaternion.identity);
                        }

                }
                break;
            default:    
                break;
        }
    }

}
