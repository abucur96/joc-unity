﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;
    public float slowness = 10f;

    public void EndGame()
    {
        if(!gameHasEnded)
        {
            gameHasEnded = true;
            StartCoroutine(SlownessCrush());
           
        }
    }

    private IEnumerator SlownessCrush()
    {
        Time.timeScale = 1f / slowness;
        Time.fixedDeltaTime /= slowness;

        yield return new WaitForSeconds(1f / slowness);

        Time.timeScale = 1f;
        Time.fixedDeltaTime *= slowness;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    

}
