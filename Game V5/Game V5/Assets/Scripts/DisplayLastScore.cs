﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DisplayLastScore : MonoBehaviour
{
    public Text finalScoreText;

    void OnEnable()
    {
        finalScoreText.text = FindObjectOfType<Score>().GetScoreText();
    }
}